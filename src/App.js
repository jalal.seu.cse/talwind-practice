import './App.css';
import Card1 from "./components/card/card-1/card_1";
import Card2 from "./components/card/card-2/card_2";

function App() {
  return (
      <div className='dark:bg-black'>
          <Card1 />
          <Card2 />
      </div>
  );
}

export default App;
