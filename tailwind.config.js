/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  darkMode: 'media',
  theme: {
    extend: {
      screens: {
        'smm': '540px',
        // => @media (min-width: 540px) { ... }

        'lgg': '1000px',
        // => @media (min-width: 1000px) { ... }

        '4k': '1200px',
        // => @media (min-width: 1200px) { ... }
      },
      colors: {
        gray: {
          300: "#D5D5D5"
        }
      },
    },
  },
  plugins: [],
}
